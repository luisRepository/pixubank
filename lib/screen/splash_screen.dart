import 'package:flutter/material.dart';
import 'package:pixubank/screen/login_screen.dart';

import 'home_screen.dart';

class Splash extends StatefulWidget {
  const Splash({Key? key}) : super(key: key);

  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 5)).then((_) => Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (context) => LoginScreen())));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.cyan[600],
      body: Center(
        child: Container(
            width: 150,
            height: 150,
            child: Image.asset(
              "assets/hamster.gif",
              height: 125.0,
              width: 125.0,
            )),
      ),
    );
  }
}
