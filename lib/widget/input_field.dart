

import 'package:flutter/material.dart';

class InputField extends StatelessWidget{

  final String label;
  final IconData icon;
  late final Map<String, dynamic> formData;
  final String campo;
  final bool obscure;

  InputField({required this.label, required this.icon, required this.formData, required this.campo, required this.obscure});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      obscureText: obscure,
      onChanged: (value){
        formData[campo] = value;
      } ,
      style: TextStyle(
      fontSize: 20, fontWeight: FontWeight.bold),
      decoration: InputDecoration(
        labelText: label,
        icon: Icon(icon)
      ),
    );
  }

}