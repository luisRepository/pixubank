import 'package:flutter/cupertino.dart';

class Usuario with ChangeNotifier {
  String usuario;
  String senha;
  String email;

  Usuario({required this.usuario, required this.senha, required this.email});
}
