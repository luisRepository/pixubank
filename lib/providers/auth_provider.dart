import 'package:flutter/material.dart';
import '../models.dart';
import '../utils/constants.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class AuthProvider with ChangeNotifier {
  final String _baseUrl = '${Contants.BASE_API_URL}';

  Future<dynamic> cadastrarUsuario(Usuario usuario) async {
    var response = await http.post(
      "$_baseUrl/api/v1/User",
      headers: {"Content-Type": "application/json"},
      body: json.encode(
        {
          "Login": usuario.usuario,
          "Password": usuario.senha,
          "Email": usuario.email
        },
      ),
    );
    notifyListeners();
    return Future<dynamic>.value(json.decode(response.statusCode.toString()));
  }
}
