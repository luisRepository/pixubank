import 'package:flutter/material.dart';
import 'package:pixubank/models.dart';
import 'package:pixubank/widget/input_field.dart';
import 'package:pixubank/widget/stagger_animation.dart';
import 'package:provider/provider.dart';
import '../providers/auth_provider.dart';
import '../utils/app_routes.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen>
    with SingleTickerProviderStateMixin {
  bool _cadastro = false;
  bool _loading = false;
  late AnimationController _animationController;
  final _form = GlobalKey<FormState>();
  final _formData = Map<String, dynamic>();

  @override
  void initState() {
    _animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 2));
    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  void ativarCadastro() {
    setState(() {
      _cadastro = !_cadastro;
    });
  }

  void cadastrar() async {
    try {
      var usuario = Usuario(
          usuario: _formData['usuario'],
          senha: _formData['senha'],
          email: _formData['email']);

      setState(() {
        Provider.of<AuthProvider>(context, listen: false)
            .cadastrarUsuario(usuario)
            .then((status) {
          if (status == 200) {
            FocusScope.of(context).requestFocus(new FocusNode());
            exibeSnack("Usuário Cadastrado com Sucesso!", Colors.green);
            _form.currentState?.reset();
            ativarCadastro();
            _animationController.reverse();
          } else if (status == 409) {
            exibeSnack("Usuário já cadastrado tente outro.", Colors.red);
          } else {
            exibeSnack(
                "Ocorreu um erro ao tentar cadastrar um usuário.", Colors.red);
          }
        });
      });
    } catch (e) {
      exibeSnack("Erro ao tentar cadastrar um usuário!", Colors.red);
    }
  }

  void logar() {
    setState(() {
      _cadastro = false;
      _loading = true;
    });
  }

  void exibeSnack(String texto, Color cor) {
    ScaffoldMessenger.of(context).removeCurrentSnackBar();
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        backgroundColor: cor,
        content: Text(texto),
        duration: Duration(seconds: 3)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Login/Cadastro"),
        centerTitle: true,
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.topRight,
                  colors: <Color>[Colors.cyan, Colors.indigo])),
        ),
      ),
      body: !_loading
          ? Padding(
              padding: const EdgeInsets.only(top: 100, left: 10, right: 10),
              child: Column(
                children: [
                  Form(
                    key: _form,
                    child: Expanded(
                      child: ListView(
                        children: [
                          Column(
                            children: [
                              InputField(
                                label: "Digite seu Usuário",
                                icon: Icons.person,
                                formData: _formData,
                                campo: "usuario",
                                obscure: false,
                              ),
                              InputField(
                                  label: "Digite sua Senha",
                                  icon: Icons.lock,
                                  formData: _formData,
                                  campo: "senha",
                                  obscure: true),
                              _cadastro == true
                                  ? InputField(
                                      label: "Digite seu Email",
                                      icon: Icons.email,
                                      formData: _formData,
                                      campo: "email",
                                      obscure: false)
                                  : Container(),
                              SizedBox(
                                height: 10,
                              ),
                              StaggerAnimation(
                                controller: _animationController,
                                action: _cadastro ? cadastrar : logar,
                                textoButton: _cadastro ? "Cadastrar" : "Logar",
                              ),
                              TextButton(
                                onPressed: ativarCadastro,
                                child: Text(
                                  "Não possuí uma conta? Cadastre-se!",
                                  style: TextStyle(fontSize: 18),
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            )
          : Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircularProgressIndicator(),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Carregando!!",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  )
                ],
              ),
            ),
    );
  }
}
