import 'package:flutter/material.dart';
import 'package:pixubank/providers/auth_provider.dart';
import 'package:pixubank/screen/splash_screen.dart';
import 'package:pixubank/utils/app_routes.dart';
import 'package:provider/provider.dart';

import 'screen/home_screen.dart';
import 'screen/login_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => new AuthProvider(),
        )
      ],
      child: MaterialApp(
        theme: ThemeData(primaryColor: Colors.cyan[600]),
        home: Splash(),
        debugShowCheckedModeBanner: false,
        routes: {
          AppRoutes.LOGIN: (ctx) => LoginScreen(),
          AppRoutes.HOME: (ctx) => Home()
        },
      ),
    );
  }
}
