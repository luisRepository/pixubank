import 'package:flutter/material.dart';

class StaggerAnimation extends StatelessWidget {
  final AnimationController controller;
  final Animation<double> buttonSqueeze;
  final Function action;
  final String textoButton;

  StaggerAnimation({required this.controller, required this.action, required this.textoButton})
      : buttonSqueeze =
            Tween<double>(begin: 320.0, end: 60.0).animate(CurvedAnimation(
                parent: controller,
                curve: Interval(
                  0.0,
                  0.150,
                )));

  Widget _buildAnimation(BuildContext context, Widget? child) {
    return Padding(
      padding: EdgeInsets.only(bottom: 5, top: 10),
      child: InkWell(
        onTap: () {
          controller.forward();
          buttonSqueeze.addStatusListener((status) {
            if (status == AnimationStatus.completed) {
              action();
            }           
          });
        },
        child: Container(
          width: buttonSqueeze.value,
          height: 40,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: Color.fromRGBO(0, 179, 179, 1.0),
              borderRadius: BorderRadius.all(Radius.circular(30))),
          child: buttonSqueeze.value > 75
              ? Text(
                  textoButton,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.w300,
                      letterSpacing: 0.3),
                )
              : CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                  strokeWidth: 1.0,
                ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: controller,
      builder: _buildAnimation,
    );
  }
}
