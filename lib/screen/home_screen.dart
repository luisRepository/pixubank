import 'package:flutter/material.dart';
import '../utils/app_routes.dart';

import 'investimentos_screen.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
    int _selectedScreenIndex = 0;
    final List<Map<String, dynamic>> _screens = [
    {'title': 'Home', 'screen': Home()},
    {'title': 'Investimentos', 'screen': InvestimentosScreen()},
  ];  

  _selectScreen(int index) {
    setState(() {
      _selectedScreenIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.cyan[100],
      appBar: AppBar(
        actions: [IconButton(onPressed: () {
          Navigator.of(context).pushReplacementNamed(AppRoutes.LOGIN);
        }, icon: Icon(Icons.login))],
        title: Text(
          "PixuBank",
          style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            padding: EdgeInsets.only(top: 10, bottom: 10),
            width: double.infinity,
            child: Stack(
              children: [
                Image.asset(
                  "assets/logobank.png",
                  height: 150.0,
                  width: 400.0,
                  alignment: Alignment.center,
                ),
                 Positioned(
                  right: 50,
                  bottom: 5,
                  child: Container(
                    width: 250,
                    color: Colors.black38,
                    padding: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
                    child: Text(
                      "Abra ja sua conta!",
                      style: TextStyle(fontSize: 26, color: Colors.white),
                      softWrap: true,
                      overflow: TextOverflow.fade,
                    ),
                  ),
                ),
              ],
            ),
          ),
                   Container(
            padding: EdgeInsets.only(top: 10, bottom: 10),
            color: Colors.cyan[100],
            width: double.infinity,
            child: Stack(
              children: [
                Image.asset(
                  "assets/logoinvestimentos.png",
                  height: 150.0,
                  width: 400.0,
                  alignment: Alignment.center,
                ),
                 Positioned(
                  right: 25,
                  bottom: 5,
                  child: Container(
                    width: 370,
                    color: Colors.black38,
                    padding: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
                    child: Text(
                      "Seu dinheiro aqui rende ++",
                      style: TextStyle(fontSize: 26, color: Colors.white),
                      softWrap: true,
                      overflow: TextOverflow.fade,
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        onTap: _selectScreen,
        backgroundColor: Theme.of(context).primaryColor,
         unselectedItemColor: Colors.white,
         selectedItemColor: Colors.black87,
         currentIndex: _selectedScreenIndex,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: "Home"
          ),
           BottomNavigationBarItem(
            icon: Icon(Icons.graphic_eq),
            label: "Investimentos"
          )
        ],
      ),
    );
  }
}
